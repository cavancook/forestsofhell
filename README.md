# Forests of Hell
###### Made by Cavan Cook
###### Loosely based of Brandon Sanderson's writing
### Basic Story
You are a member of the I.R.E. a group in the universe trying to keep it from falling apart. You have been informed that the shadows of threnody are beging to excape the planet and are reaking havoc your job is to find out why and find a way to contain them.

### Gameplay
* Use wasd keys to move and mouse to aim
* E to interact: Open, Close, Activate
* P to pause game
* Right click to ADS
* Enemies: shadows and fort folk
* Each level completed saves game

### Space
* The environment of the game is that of dark forests haunted by shadows that are argessive on movement, and enraged on the drawing of blood. As well as small village in forest protected by silver.


### Goals
* Winning condition: By surviving, finding research by local scientist and by stoping the exacpe of the shadows
* Losing condition: By getting killed by the shadows, or failing to stop thier excape
* Goals: gather research, upgrade weapon and stop/destroy shadows

### Actors
* Main character: World hopper that has traveled to threnody
* Enemies: Shadows - violent and ruthless entities, fort folk - humans with differing beliefs and morals
